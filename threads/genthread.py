import threading

from ctab.models import Tasks, DescripModelForm
from datetime import datetime
import re, shlex

# Importacion para llamar a comandos
from django.core.management import call_command
from django.utils import timezone

import logging
# Get an instance of a logger
logger = logging.getLogger(__name__)




class TasksThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
        threading.Thread.__init__(self, group=group, target=target, name=name, verbose=verbose)
        self.args = args
        self.kwargs = kwargs
        return
    
    def run(self):
        task_id = self.kwargs['task_id']
        
        logger.info("Lanzamos el proceso {}".format(task_id))
        task_ejecutable = Tasks.objects.get(id=task_id)
        command, tOption = task_ejecutable.task.split(" ",1)
        logger.info("Comprobamos que no esta la particula: {}".format(tOption))
        if "{author}" in tOption:
	        logger.info("Cambiamos por {}".format(task_ejecutable.author))
                tOption = tOption.format(author=task_ejecutable.author)
        options = shlex.split(tOption)
        logger.debug("{} {}".format(command,options))
        try:
            call_command(command,*options)
            task_ejecutable.ultima = timezone.now()
            task_ejecutable.save()
        
        except Exception, e:
            logger.error("ERROR EN LA TAREA {} : {}".format(task_ejecutable.descrip, e.message), exc_info=True)

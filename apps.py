from django.apps import AppConfig


class CtabConfig(AppConfig):
    name = 'ctab'
